# Analysis

This editorial is borrowed from the official codeforces editorial. (Because they explain it better than I can)

Observe that we can go from cell $`(r, c)`$ to cell $`(r \pm 1, c)`$, only if the parity of $`R[r]`$ and the target cell (i.e. $`R[\pm 1]`$) are the same because there is only one element
changing, i.e. $`R[r]`$ to $`R[r \pm 1]`$. Similar things also happened with the column. We can go from cell $`(r, c)`$ to cell $`(r, c \pm 1)`$ only if the parity of $`C[c]`$ and the
target cell (i.e. $`C[c \pm 1]`$) are the same.

Therefore, for each query $`(r_a, c_a, r_b, c_b)`$, we only need to check whether the parity of $`R[r]`$ are the same for all $`r = \min(r_a, r_b) \cdots \max(r_a, r_b)`$, and 
whether the parity of $`C[c]`$ are the same for all $`c = \min(c_a, c_b) \cdots \max(c_a, c_b)`$. Do a precomputation first (in $`O(N)`$ using a prefix sum array) before processing
any quey so we can decide whether $`R[i \cdots j]`$ or $`C[i \cdots j]`$ have the same parity in $`O(1)`$ for all pair of $`i`$ and $`j`$. 

Here, `parity` means **odd** or **even**. And a number that is **even** can only be the result of $`2`$ **even** numbers or $`2`$ **odd** numbers. Also, make sure to swap $`r_a, r_b`$
and $`c_a, c_b`$ if they are out of place when calculating using prefix sum array, as it messes up your calculations. (Meaning that $`r_a`$ should be smaller than $`r_b`$, and the same
thing for $`c_a`$ and $`c_b`$).

You can use a union-find data structure to do this, although its over kill; there is another much simpler ad-hoc method to find the groups involving only one iteration.

**Time Complexity:** $`O(N + Q)`$