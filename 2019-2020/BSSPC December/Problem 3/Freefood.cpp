#include<bits/stdc++.h>
using namespace std;

bool freefood[366];
int N;

int main() {
    cin >> N;
    for(int i=0, st, ed; i<N; i++) {
        cin >> st >> ed;
        for(int j=st; j<=ed; j++) {
            freefood[j] = true;
        }
    }    
    int cnt=0;
    for(int i=1; i<=365; i++) {
        if(freefood[i]) {
            cnt++;
        }
    }
    cout << cnt << endl;
}