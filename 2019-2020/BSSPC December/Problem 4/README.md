# Analysis

This is a straight forward dijkstras problem. Simply build the graph, then just run dijkstras **ONCE** from the starting node $`U`$. Then just for each query, just query from
your distance array, remember, dijkstras calculates the distance for **ALL** nodes from the starting node. 

Remember to print `NO PATH` if the distance is infinite (or the large value you set the distance array to)

**Time Complexity:** $`O(N^2)`$