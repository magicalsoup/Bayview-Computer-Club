# Analysis

Lets write a short proof here, a number that is even can always be split into 2 even numbers, except the number $`2`$

If $`\dfrac{N}{2}`$ results in two even numbers, we are done.

If $`\dfrac{N}{2}`$ results in two odd numbers, we can always subtract one from one number and add one to the other number. Realize one of the numbers will be non-positive after this, <br> hence why $`N = 2`$ does not work here.

Therefore, we just need to check if the number is divisible 2 and is not $`2`$ and print `YES`, and print `NO` otherwise.

**Time Complexity:** $`O(1)`$

