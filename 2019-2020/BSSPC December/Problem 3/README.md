# Analysis

## Brute-Force
Since the bounds are small, this method works.

For each of the $`N`$ events, we just loop through and set each of the days from $`s_i`$ to $`t_i`$ inclusive to `true`.

After marking all the days that have freefood, we just loop from $`1`$ to $`365`$ and check which days there is freefood, here we just array indexes to represent days.

Print the final answer when you are done.

**Time Complexity:** $`O(N^2)`$


## Difference Array

Alternatively, you could have use difference array to update a range, and then doing a prefix sum array after the updates, and then seeing which array index has a value greater than $`0`$.

**Time Complexity:** $`O(N)`$
