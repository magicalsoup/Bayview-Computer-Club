#include <bits/stdc++.h>
using namespace std;
#define INF 0x3f3f3f3f
#define NINF -INF
#define mp make_pair
#define boost() cin.tie(0); cin.sync_with_stdio(0)
typedef long long ll;
typedef long double ld;
typedef int64_t l;
typedef pair<int, int> pii;
typedef pair<int, ll> pil;
typedef pair<ll, ll> pll;
const int MOD = 1000000007;
const int MAXN = 100005, MAXQ = 100005;
ll gcd(ll a, ll b){return b == 0 ? a : gcd(b, a % b);}
ll lcm(ll a, ll b){return a*b/gcd(a,b);}
ll fpow(ll  b, ll exp, ll mod){if(exp == 0) return 1;ll t = fpow(b,exp/2,mod);if(exp&1) return t*t%mod*b%mod;return t*t%mod;}
ll divmod(ll i, ll j, ll mod){i%=mod,j%=mod;return i*fpow(j,mod-2,mod)%mod;}
 
int N, Q, psar[MAXN], psac[MAXN];
int main() {
    boost();
    cin>>N>>Q;
    for(int i=1, x; i<=N; i++) {
        cin>>x; psar[i] = psar[i-1] + (x%2==0);
    }
    for(int i=1, x; i<=N; i++) {
        cin>>x; psac[i] = psac[i-1] + (x%2==0);
    }
    while(Q--) {
        int ra, ca, rb, cb; cin>>ra>>ca>>rb>>cb;
        if(ra > rb) swap(ra, rb); if(ca > cb) swap(ca, cb);
        if((psar[rb] - psar[ra-1] == 0 && psac[cb] - psac[ca-1] == 0) || (psar[rb] - psar[ra-1] == rb-ra+1 && psac[cb] - psac[ca-1] == cb - ca + 1))
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
 
}