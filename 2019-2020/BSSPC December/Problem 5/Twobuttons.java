import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;
 
public class Twobuttons {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out));
    static StringTokenizer st;
    static boolean vis[] = new boolean[100005];
    static HashSet<Long> s = new HashSet<>();
    public static void main(String[]args) throws IOException{
        int n = readInt(), m = readInt();
        bfs(n, m);
    }
    static void bfs(int st, int ed) {
        LinkedList<pair> q = new LinkedList<>();
        q.add(new pair(st, 0));
        while(!q.isEmpty()) {
            pair cur = q.poll();
            if(cur.nxt == ed) {
                System.out.println(cur.moves);
                return;
            }
            if(cur.nxt < 0 || s.contains(cur.nxt)) continue;
            s.add(cur.nxt);
            q.add(new pair(cur.nxt-1, cur.moves+1));
            if(cur.nxt > 2*ed) continue;
            q.add(new pair(cur.nxt*2, cur.moves+1));
        }
    }
    static class pair {
        long nxt; int moves;
        public pair(long nxt, int moves) {
            this.nxt=nxt;
            this.moves=moves;
        }
    }
    static String next() throws IOException {
        while(st == null || !st.hasMoreTokens())
            st = new StringTokenizer(br.readLine().trim());
        return st.nextToken();
    }
    static int readInt() throws IOException {
        return Integer.parseInt(next());
    }
    static String readLine() throws IOException {
        return br.readLine().trim();
    }
}