#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> pii;
int N, U, Q;

vector<pii> adj[502]; int dis[502];

void dijkstras() {
    memset(dis, 0x3f, sizeof dis);
    queue<pii> q; q.push({U, 0});
    dis[U] = 0;
    while(!q.empty()) {
        pii cur = q.front(); q.pop();
        for(pii v : adj[cur.first]) {
            if(cur.second + v.second < dis[v.first]) {
                dis[v.first] = cur.second + v.second;
                q.push({v.first, dis[v.first]});
            }
        }
    }
}

int main() {
    cin >> N;
    for(int i=0; i<N; i++) {
        int a, b, w;
        cin >> a >> b >> w;
        adj[a].push_back({b, w});
        adj[b].push_back({a, w});
    }
    cin >> U;
    dijkstras();
    cin >> Q;
    for(int i=0; i<Q; i++) {
        int v; cin >> v;
        if(dis[v] == 0x3f3f3f3f) {
            cout << "NO PATH" << endl;
        }
        else {
            cout << dis[v] << endl;
        }   
    }
}